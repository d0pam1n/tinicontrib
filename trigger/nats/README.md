<!--
title: NATS
weight: 4701
-->

# NATS Trigger

This trigger subscribes to a topic on NATS cluster and listens for the messages.

## Configuration

### Setting :

| Name       | Type   | Description                                                                                                                                                                                           |
| :--------- | :----- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| brokerUrls | string | The brokers of the NATS cluster to connect to - **_REQUIRED_**                                                                                                                                        |
| user       | string | If connecting to a SASL enabled port, the userid to use for authentication                                                                                                                            |
| password   | string | If connecting to a SASL enabled port, the password to use for authentication                                                                                                                          |
| trustStore | string | If connecting to a TLS secured port, the directory containing the certificates representing the trust chain for the connection. This is usually just the CACert used to sign the server's certificate |

### HandlerSettings:

| Name  | Type   | Description                                    |
| :---- | :----- | :--------------------------------------------- |
| topic | string | The NATS topic on which to listen for messages |

### Output:

| Name    | Type   | Description                                    |
| :------ | :----- | :--------------------------------------------- |
| subject | string | The subject the message that was consumed from |
| message | string | The message that was consumed                  |

## Examples

```json
{
  "triggers": [
    {
      "id": "flogo-nats",
      "ref": "gitlab.com/tini/contrib/trigger/nats",
      "settings": {
        "brokerUrls": "localhost:4222",
        "trustStore": ""
      },
      "handlers": [
        {
          "settings": {
            "topic": "syslog"
          },
          "action": {
            "ref": "github.com/project-flogo/flow",
            "settings": {
              "flowURI": "res://flow:my_flow"
            }
          }
        }
      ]
    }
  ]
}
```

## Development

### Testing

To run tests first set up the nats broker using the docker like given below:

```bash
docker run -d --rm --name nats -p 4222:4222 nats -DV
```

Then run the following command:

```bash
go test
```
