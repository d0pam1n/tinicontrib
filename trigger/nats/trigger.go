package nats

import (
	"context"
	nats "github.com/nats-io/nats.go"
	"github.com/project-flogo/core/data/metadata"
	"github.com/project-flogo/core/support/log"
	"github.com/project-flogo/core/trigger"
	"strings"
	"time"
)

var triggerMd = trigger.NewMetadata(&Settings{}, &HandlerSettings{}, &Output{})

func init() {
	_ = trigger.Register(&Trigger{}, &Factory{})
}

// Trigger contains the base settings of the trigger
type Trigger struct {
	handlers   map[string]*clientHandler
	settings   *Settings
	logger     log.Logger
	options    *nats.Options
	connection *nats.Conn
}

type clientHandler struct {
	handler      trigger.Handler
	subscription *nats.Subscription
	settings     *HandlerSettings
}

// Factory is a factory for multiple triggers
type Factory struct {
}

// Metadata returns the metadata of the trigger
func (*Factory) Metadata() *trigger.Metadata {
	return triggerMd
}

// New implements trigger.Factory.New
func (*Factory) New(config *trigger.Config) (trigger.Trigger, error) {
	s := &Settings{}

	err := metadata.MapToStruct(config.Settings, s, true)
	if err != nil {
		return nil, err
	}

	return &Trigger{settings: s}, nil
}

// Initialize initializes the trigger
func (t *Trigger) Initialize(ctx trigger.InitContext) error {
	t.logger = ctx.Logger()

	settings := t.settings
	options := initClientOption(settings)
	t.options = options

	t.logger.Debugf("Client options: %v", options)

	t.handlers = make(map[string]*clientHandler)

	for _, handler := range ctx.GetHandlers() {

		s := &HandlerSettings{}
		err := metadata.MapToStruct(handler.Settings(), s, true)
		if err != nil {
			return err
		}

		t.handlers[s.Topic] = &clientHandler{handler: handler, settings: s}
	}

	return nil
}

// Start starts the trigger
func (t *Trigger) Start() error {
	nc, err := nats.Connect(strings.Join(t.options.Servers, ","), nats.ReconnectWait(10*time.Second),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			t.Subscribe()
		}))
	if err != nil {
		t.logger.Errorf("Could not connect to server: %s", err)
		return err
	}

	t.connection = nc
	if err := t.Subscribe(); err != nil {
		return err
	}

	return nil

}

// Stop stops the trigger
func (t *Trigger) Stop() error {
	for _, handler := range t.handlers {
		err := handler.subscription.Unsubscribe()

		if err != nil {
			t.logger.Errorf("Error unsubscribing from topic: %s", handler.settings.Topic)
			return err
		}
	}

	t.connection.Close()

	return nil
}

// Subscribe adds the subscription to the configured topics.
func (t *Trigger) Subscribe() error {
	for _, handler := range t.handlers {
		sub, err := t.connection.Subscribe(handler.settings.Topic, func(m *nats.Msg) {
			handler.handler.Handle(context.Background(), &Output{
				Subject: m.Subject,
				Message: string(m.Data),
			})
		})

		if err != nil {
			t.logger.Errorf("Error subscribing to topic: %s", handler.settings.Topic)
			return err
		}

		handler.subscription = sub
	}

	return nil
}

func initClientOption(settings *Settings) *nats.Options {
	opts := nats.Options{}

	opts.Servers = prepareBrokers(settings.BrokerUrls)
	opts.User = settings.User
	opts.Password = settings.Password

	return &opts
}

func prepareBrokers(brokerUrls string) []string {
	const protocol = "nats://"
	brokers := strings.Split(brokerUrls, ",")
	var preparedBrokers []string

	for _, broker := range brokers {
		preparedBrokers = append(preparedBrokers, protocol+broker)
	}

	return preparedBrokers
}
