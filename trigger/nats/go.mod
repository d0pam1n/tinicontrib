module gitlab.com/d0pam1n/tinicontrib/trigger/nats

require (
	github.com/nats-io/nats-server/v2 v2.1.2 // indirect
	github.com/nats-io/nats.go v1.9.1
	github.com/project-flogo/core v0.9.4
	github.com/stretchr/testify v1.3.0
)

go 1.13
