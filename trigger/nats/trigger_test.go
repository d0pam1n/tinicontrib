package nats

import (
	"encoding/json"
	_ "net"
	_ "os"
	_ "os/exec"
	"testing"
	"time"

	"github.com/project-flogo/core/action"
	"github.com/project-flogo/core/support"
	"github.com/project-flogo/core/support/test"
	"github.com/project-flogo/core/trigger"
	"github.com/stretchr/testify/assert"

	nats "github.com/nats-io/nats.go"
)

/*func Pour(port string) {
	for {
		conn, _ := net.Dial("tcp", net.JoinHostPort("", port))
		if conn != nil {
			conn.Close()
			break
		}
	}
}

func TestMain(m *testing.M) {
	// TODO: Start container with docker sdk
	command := exec.Command("docker", "start", "nats")
	err := command.Run()
	if err != nil {
		command := exec.Command("docker", "run", "-p", "4222:4222", "--name", "nats", "-d", "nats")
		err := command.Run()
		if err != nil {
			panic(err)
		}
	}
	Pour("4222")
	os.Exit(m.Run())
}*/

const testConfig string = `{
	"id": "trigger-nats",
	"ref": "gitlab.com/d0pam1n/final/trigger/nats",
	"settings": {
        "brokerUrls": "localhost:4222"
    },
	"handlers": [
	  {
		"settings": {
			"topic": "test"
		},
		"action" : {
		  "id": "dummy"
		}
	  }
	]
  }`

const testConfigLocal string = `{
	"id": "trigger-nats",
	"ref": "gitlab.com/d0pam1n/final/trigger/nats",
	"settings": {
		"brokerUrls": "localhost:4222"
	},
	"handlers": [
	  {
		"settings": {
			"topic": "test"
		},
		"action" : {
		  "id": "dummy"
		}
	  }
	]
  }`

const testConfigLocalStart string = `{
	"id": "trigger1-nats",
	"ref": "gitlab.com/d0pam1n/final/trigger/nats",
	"settings": {
		"brokerUrls": "localhost:4222"
	},
	"handlers": [
	  {
		"settings": {
			"topic": "test"
		},
		"action" : {
		  "id": "dummyTest"
		}
	  }
	]
  }`

func TestTrigger_Register(t *testing.T) {
	ref := support.GetRef(&Trigger{})
	f := trigger.GetFactory(ref)
	assert.NotNil(t, f)
}

func TestTrigger_Initialize(t *testing.T) {
	f := &Factory{}

	config := &trigger.Config{}
	err := json.Unmarshal([]byte(testConfigLocal), config)
	assert.Nil(t, err)

	actions := map[string]action.Action{"dummy": test.NewDummyAction(func() {
	})}

	trg, err := test.InitTrigger(f, config, actions)
	assert.Nil(t, err)
	assert.NotNil(t, trg)

	err = trg.Start()
	assert.Nil(t, err)

	err = trg.Stop()
	assert.Nil(t, err)
}

func TestTrigger_Start(t *testing.T) {
	f := &Factory{}

	config := &trigger.Config{}
	err := json.Unmarshal([]byte(testConfigLocalStart), config)
	assert.Nil(t, err)

	done := make(chan bool, 1)
	actions := map[string]action.Action{"dummyTest": test.NewDummyAction(func() {
		done <- true
	})}

	trg, err := test.InitTrigger(f, config, actions)
	assert.Nil(t, err)
	assert.NotNil(t, trg)

	err = trg.Start()
	assert.Nil(t, err)

	nc, err := nats.Connect("nats://localhost:4222")
	assert.Nil(t, err)

	err = nc.Publish("test", []byte("Hello World"))
	assert.Nil(t, err)

	select {
	case <-done:
	case <-time.Tick(time.Second):
		t.Fatal("Did not get message in time")
	}

	nc.Close()

	err = trg.Stop()
	assert.Nil(t, err)
}
