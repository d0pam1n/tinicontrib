package nats

import (
	nats "github.com/nats-io/nats.go"
	"github.com/project-flogo/core/activity"
	"github.com/project-flogo/core/data/metadata"
	"strings"
	"time"
)

var activityMd = activity.ToMetadata(&Settings{}, &Input{})

func init() {
	_ = activity.Register(&Activity{}, New)
}

// New creates a new activity context
func New(ctx activity.InitContext) (activity.Activity, error) {
	settings := &Settings{}
	err := metadata.MapToStruct(ctx.Settings(), settings, true)
	if err != nil {
		return nil, err
	}

	options := initClientOption(settings)

	nc, err := nats.Connect(strings.Join(options.Servers, ","), nats.ReconnectWait(10*time.Second))
	if err != nil {
		return nil, err
	}

	act := &Activity{
		connection: nc,
		settings:   settings,
	}

	return act, nil
}

// Activity settings
type Activity struct {
	settings   *Settings
	connection *nats.Conn
}

// Metadata returns the metadata of the activity
func (a *Activity) Metadata() *activity.Metadata {
	return activityMd
}

// Eval gets called when a new message arrives
func (a *Activity) Eval(ctx activity.Context) (done bool, err error) {
	input := &Input{}

	err = ctx.GetInputObject(input)
	if err != nil {
		return true, err
	}

	if err := a.connection.Publish(a.settings.Topic, input.Message); err != nil {
		ctx.Logger().Debugf("Error publishing: %v", err)
		return true, err
	}

	return true, nil
}

func initClientOption(settings *Settings) *nats.Options {
	opts := nats.Options{}

	opts.Servers = prepareBrokers(settings.BrokerUrls)
	opts.User = settings.User
	opts.Password = settings.Password

	return &opts
}

func prepareBrokers(brokerUrls string) []string {
	const protocol = "nats://"
	brokers := strings.Split(brokerUrls, ",")
	var preparedBrokers []string

	for _, broker := range brokers {
		preparedBrokers = append(preparedBrokers, protocol+broker)
	}

	return preparedBrokers
}
