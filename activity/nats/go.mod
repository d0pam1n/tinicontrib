module gitlab.com/d0pam1n/tinicontrib/activity/nats

go 1.13

require (
	github.com/nats-io/nats.go v1.9.1
	github.com/project-flogo/core v0.9.4
	github.com/stretchr/testify v1.3.0
)
