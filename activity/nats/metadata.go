package nats

import (
	"github.com/project-flogo/core/data/coerce"
)

// Settings stores the basic configuration to connect to the NATS cluster
type Settings struct {
	BrokerUrls string `md:"brokerUrls,required"` // The NATS cluster to connect to
	User       string `md:"user"`                // If connecting to a SASL enabled port, the user id to use for authentication
	Password   string `md:"password"`            // If connecting to a SASL enabled port, the password to use for authentication
	TrustStore string `md:"trustStore"`          // If connecting to a TLS secured port, the directory containing the certificates representing the trust chain for the connection. This is usually just the CACert used to sign the server's certificate
	Topic      string `md:"topic,required"`      // The topic to publish to
}

// Input stores the message to send.
type Input struct {
	Message []byte `md:"message"`
}

// ToMap deserizalizes the Input to a struct
func (i *Input) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"message": i.Message,
	}
}

// FromMap serizalizes a map to the Input fields.
func (i *Input) FromMap(values map[string]interface{}) error {
	var err error
	i.Message, _ = coerce.ToBytes(values["message"])
	if err != nil {
		return err
	}
	return nil
}
