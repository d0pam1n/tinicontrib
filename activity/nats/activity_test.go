package nats

import (
	_ "encoding/json"
	_ "net"
	_ "os"
	_ "os/exec"
	"testing"
	"time"

	nats "github.com/nats-io/nats.go"
	"github.com/project-flogo/core/activity"
	"github.com/project-flogo/core/support/test"
	"github.com/stretchr/testify/assert"
)

/*func Pour(port string) {
	for {
		conn, _ := net.Dial("tcp", net.JoinHostPort("", port))
		if conn != nil {
			conn.Close()
			break
		}
	}
}

func TestMain(m *testing.M) {
	// TODO: Start container with docker sdk.
	command := exec.Command("docker", "start", "nats")
	err := command.Run()
	if err != nil {
		command := exec.Command("docker", "run", "-p", "4222:4222", "--name", "nats", "-d", "nats")
		err := command.Run()
		if err != nil {
			panic(err)
		}
	}
	Pour("4222")
	os.Exit(m.Run())
}*/

func TestRegister(t *testing.T) {
	ref := activity.GetRef(&Activity{})
	act := activity.Get(ref)

	assert.NotNil(t, act)
}

func TestEval(t *testing.T) {
	nc, err := nats.Connect("nats://localhost:4222")
	assert.Nil(t, err)

	fini := make(chan bool, 1)

	sub, err := nc.Subscribe("test", func(m *nats.Msg) {
		topic, message := m.Subject, string(m.Data)
		assert.Equal(t, "hello world", message)
		assert.Equal(t, "test", topic)
		fini <- true
	})
	assert.Nil(t, err)

	settings := Settings{
		BrokerUrls: "localhost:4222",
		Topic:      "test",
	}
	init := test.NewActivityInitContext(settings, nil)
	act, err := New(init)
	assert.Nil(t, err)

	context := test.NewActivityContext(activityMd)
	context.SetInput("message", "hello world")

	done, err := act.Eval(context)
	assert.True(t, done)
	assert.Nil(t, err)

	select {
	case <-fini:
	case <-time.Tick(time.Second):
		t.Fatal("Did not get message in time")
	}

	sub.Unsubscribe()
}
